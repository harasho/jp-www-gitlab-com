---
layout: handbook-page-toc
title: Documentation
---

GitLab documentation is crafted to help users, admins, and decision-makers
learn about GitLab features and to optimally implement and use GitLab to meet
their [DevOps needs](/stages-devops-lifecycle/).

The documentation is an essential part of the product. Its source is developed
and stored with the product in its respective paths within the
[GitLab repositories](https://docs.gitlab.com/ee/development/documentation/site_architecture/#architecture).
It's published at [docs.gitlab.com](https://docs.gitlab.com) (offering multiple
versions of all product documentation) and at the `/help/` path on each GitLab
instance’s domain, with content for that instance’s version.

Our goal is to create documentation that is accurate and easy
to use. The documentation should be easy to browse or search for the information you need, and
it should be easy to contribute to the documentation itself.

All standards and practices for contributing documentation are in the
[GitLab Documentation guidelines](https://docs.gitlab.com/ee/development/documentation/).

## 目次 {#on-this-page}
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## ドキュメントを唯一の情報源とする (SSOT) {#documentation-is-the-single-source-of-truth-ssot}

See the [SSOT section in the Documentation Style Guide](https://docs.gitlab.com/ee/development/documentation/styleguide/#documentation-is-the-single-source-of-truth-ssot),
and especially read the [docs-first methodology](https://docs.gitlab.com/ee/development/documentation/styleguide/#docs-first-methodology).

## ドキュメントへの貢献 {#contributing-to-the-documentation}

Everyone can contribute. For documentation needs resulting from the development
and release of new or enhanced features, the developer responsible for the code
writes or updates the documentation.

Technical writers monitor the planning and merging of documentation, reviewing
all changes before or after merging.

For more information on these processes, see the
[Documentation section of our Development documentation](https://docs.gitlab.com/ee/development/documentation/).

## ドキュメントをマージする {#merging-documentation}

ドキュメンテーションは、Maintainer 権限を持つ人であれば誰でも、ドキュメンテーションの変更を 
GitLab master または main ブランチにマージすることができます。

- 想定している読者にとって明確で十分に理解しやすいこと。
- 技術的に正確であること（自分の知識または著者やSME(Subject Mater Exper該当領域の専門家)レビュアーへの信頼に基づいたものであること）
- GitLab [ドキュメンテーションガイドライン](https://docs.gitlab.com/ee/development/documentation/)および[スタイルガイド](https://docs.gitlab.com/ee/development/documentation/styleguide/)に沿っていること。

GitLabのテクニカルライターは、すべてのコンテンツをレビューし、それがわかりやすくで、構造やスタイルのガイドラインに適合していることを確認し、場合によってはさらに改善します。

なお、ドキュメントの構成やスタイルが完璧でなくてもマージは可能です。
まずは改善されたドキュメントを公開しその後で修正した方が、レビュー中のまま非公開にしておくよりもよいです。非公開のままでは、他の人は、改善されたドキュメントの存在を知り得ないし、古いバージョンのドキュメントを使うことになってしまいます。

しかし、ドキュメントページがマージされた後でも、追加作業や改善が必要だと感じた場合は、別のMRやIssueを作成してください。

詳しくは、[開発ドキュメントのガイドライン](https://docs.gitlab.com/ee/development/documentation/)をご覧ください。

## GitLab ドキュメントに関する資料 {#resources-about-gitlab-documentation}

- [ドキュメンテーションガイドライン](https://docs.gitlab.com/ee/development/documentation/):
    - [ワークフロー](https://docs.gitlab.com/ee/development/documentation/workflow.html)
    - [ページ構成とテンプレート](https://docs.gitlab.com/ee/development/documentation/structure.html)
    - [スタイルガイド](https://docs.gitlab.com/ee/development/documentation/styleguide/)
    - [サイト構成](https://docs.gitlab.com/ee/development/documentation/site_architecture/index.html)
- [Markdown ガイド](/handbook/markdown-guide/)
- [GitLab Docsプロジェクト](https://gitlab.com/gitlab-org/gitlab-docs/)、複数のリポジトリからドキュメントを抽出するコードと、docs.gitlab.comを構築するコードが含まれています。

## テクニカルライティングチーム {#the-technical-writing-team}

For more information about the team and how we continually improve
the GitLab documentation, see [Technical Writing](../product/technical-writing/)
in the Product section of the handbook.
