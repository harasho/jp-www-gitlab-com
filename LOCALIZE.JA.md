# jp-www-gitlab-com

[GitLabのハンドブック](https://about.gitlab.com/handbook/)の、[日本語訳](https://jp-handbook-gitlab.tech4u.dev/handbook/)サイトのソースコードです。

## 解説

### 開発方法
[コチラ](doc/development.md)を読んで下さい

```sh
# リポジトリから標準のMiddlemanプレビューサーバを実行します。
alias serve_handbook="cd /path/to/www-gitlab-com/sites/handbook; bundle exec middleman serve"

# 契約なしでプレビューサーバーを実行 - より高速なサービス、完全なミドルマン機能はそのままです。
alias serve_handbook_faster="cd /path/to/www-gitlab-com/sites/handbook; NO_CONTRACTS=true bundle exec middleman serve" 

# サイトマップの再構築を行わず、contracts なしでプレビューサーバーを実行 - 高速な配信、迅速なテンプレート開発、一部のMiddleman機能が動作しない場合があります。
alias serve_handbook_fastest="cd /path/to/www-gitlab-com/sites/handbook; DISABLE_ON_DISK_REBUILD=true NO_CONTRACTS=true bundle exec middleman serve"

# サイトマップの再構築を行わず、contracts なし､ライブリロード有効で､プレビューサーバを実行 - ライブリロードは大幅に速度を落とすかもしれませんが、より便利になるかもしれません。
alias serve_handbook_fastest_livereload="cd /path/to/www-gitlab-com/sites/handbook;DISABLE_ON_DISK_REBUILD=true ENABLE_LIVERELOAD=1 NO_CONTRACTS=true bundle exec middleman serve"
```

```sh
alias serve_handbook_jp="cd /home/workspaces/jp-www-gitlab-com/sites/handbook; DISABLE_ON_DISK_REBUILD=true NO_CONTRACTS=true bundle exec middleman serve" 
```

## 見出しの変換用の正規表現

`npm run localize-header -- --path sites/uncategorized/source/company/culture/all-remote/effective-communication/index.html.md.erb`

## 用語集

|英語|日本語|
|--|--|
|On this page|目次 |