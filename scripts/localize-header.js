const { program } = require('commander');
const { readFileSync, writeFileSync } = require('fs');

function main(args) {
  program.requiredOption('-p, --path <glob-pattern>', 'Path of the markdown files to format');
  program.parse(args);
  const md = readFileSync(program.path).toString();
  const cv = md.
  replace(/^(#+) ((?!<%=).+)$/gm, (m, g1, g2) => `${g1} ${g2} {#${replacer(g2)}}`).
  replace(/^(#+ .+ \{#.+) (.+\})$/gm, '$1-$2').
  replace(/^(#+ .+ \{#.+)[^A-Za-z0-9-](.*\})$/gm, '$1$2');
  writeFileSync(program.path, cv);
}

function replacer(g2) {
  return g2.toLowerCase().replaceAll(' ', '-').replace(/[^A-Za-z0-9-]/,'')
}
main(process.argv);
